.data
string1: .asciiz "Enter an integer: "
string2: .asciiz "\nEnter another integer: "
string3: .asciiz "\nThe greatest common divisor is:"

.text

li $v0,4
la $a0,string1
syscall

li $v0, 5
syscall
add $s0,$v0,$zero  

li $v0,4
la $a0,string2
syscall

li $v0, 5
syscall
add $s1,$v0,$zero   




slt $t0,$s0,$s1
beq $t0,$zero,L    
add $t1,$s0,$zero
add $s0,$s1,$zero
add $s1,$t1,$zero

L: 
add $a0,$s0,$zero   
add $a1,$s1,$zero   



jal gcdRec

gcdRec:

addi $sp,$sp,-4
sw $ra,0($sp)


bne $a1,$zero,L1  

add $v0,$zero,$a0  
addi $sp,$sp,4
jr $ra


L1:
div $a0,$a1
add $a0,$a1,$zero
mfhi $a1
jal gcdRec


lw $ra,0($sp)
addi $sp,$sp,4

jr $ra


add $s3,$v0,$zero  


li $v0,4
la $a0,string3
syscall


li $v0,1
add $a0,$s3,$zero
syscall